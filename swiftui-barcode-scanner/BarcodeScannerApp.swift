//
//  swiftui_barcode_scanner_ios_15App.swift
//  swiftui-barcode-scanner-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

@main
struct BarcodeScannerApp: App {
    var body: some Scene {
        WindowGroup {
            BarcodeScannerView()
        }
    }
}
