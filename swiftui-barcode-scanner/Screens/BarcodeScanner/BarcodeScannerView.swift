//
//  ContentView.swift
//  swiftui-barcode-scanner-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

struct BarcodeScannerView: View {
    @StateObject var viewModel = BarcodeScannerViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                ScannerView(scannedCode:  $viewModel.scannedCode, alertItem: $viewModel.alertItem)
                    // maxWidth default is .infinity
                    //.frame(maxWidth: .infinity, maxHeight: 300)
                    .frame(maxHeight: 300)
                
                Spacer()
                    .frame(height: 60)
                
                Label("Scanned Barcode: ", systemImage: "barcode.viewfinder")
                    .font(.title)
                
                Text(viewModel.statusText)
                    .bold()
                    .font(.largeTitle)
                    .foregroundColor(viewModel.statusTextColor)
                    .padding()

            }
            .navigationTitle("Barcode Scanner")
            .alert(item: $viewModel.alertItem) { item in
                Alert(title: item.title, message: item.message, dismissButton: item.dismissButton)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        BarcodeScannerView()
    }
}
